# BLADE IR

This work was commissioned during the Search Engine course to experiment with Informational Retrieval techniques and approach. To test this repository make sure 
you have the following files in the experiment folder:


    1) parliamentary.json
    2) debateorg.json
    3) debatepedia.json
    4) idebate.json
    5) debatewise.json


    

__Note__: you can also use only the parliamentary.json by answering no to the question "Do you want to index all documents?"

All files are present at the following address: https://zenodo.org/record/3734893#.YIvxtbUzbIV 

Inside the experiment folder there is also an XML file called topics-task-2.xml, it contains the topics provided by the 2020 Touch� competition.


## FIRST USE

When you run the program you will be asked the following questions:

    Do you want to clean the index folder? (Press 1 if you want to clean the index folder, 0 otherwise)
    Do you want to index all documents? (1 = yes, 0 = no (only parliamentary.json))
    Do you want to print the retrieved argument in out.txt? (1 = yes, 0 = no)
    Do you want to search for each topic? (1 = yes, 0 = no)

If you want to index only parliamentary.json then press 0 to the second question, otherwise all documents will be indexed.

Indexing all documents takes about 10 minutes


## BLADE IR TEAM

    Benedetti Matteo
    Ghedin Filippo
    Romanello Mattia
    Romanello Stefano
    Rossi Leonardo
   
    