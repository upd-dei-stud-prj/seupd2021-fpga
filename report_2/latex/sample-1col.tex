\documentclass[]{ceurart}
\usepackage{array}


\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{multicol}
\newcolumntype{L}{>{\centering\arraybackslash}m{3cm}}

\begin{document}

\copyrightyear{2021}
\copyrightclause{Copyright for this paper by its authors.
  Use permitted under Creative Commons License Attribution 4.0
  International (CC BY 4.0).}

\conference{CLEF'21: Conference and Labs of the Evaluation Forum, September 21--24, 2021, Bucharest, Romania}

\title{\centering Blade solution for Argument Retrieval}
\title[mode=sub]{\hskip 4em Notebook for the Touch{\'e} Lab on Argument Retrieval at CLEF 2021}

\author[1]{Romanello Mattia}[email=mattia.romanello@studenti.unipd.it ]
\author[1]{Romanello Stefano}[email=stefano.romanello.1@studenti.unipd.it]
\author[1]{Rossi Leonardo}[email=leonardo.rossi.3@studenti.unipd.it]
\author[1]{Ghedin Filippo}[email=filippo.ghedin@studenti.unipd.it]
\author[1]{Benedetti Matteo}[email=matteo.benedetti.4@studenti.unipd.it]
\address[1]{University of Padua, Via Giovanni Grodenigo 6, Padua, Italy}

\begin{abstract}
  Support users who search for arguments to be used in conversations (e.g., getting an overview of pros and cons or just looking for arguments in line with a user's stance). Given a question on a controversial topic, the task is to retrieve relevant arguments from a focused crawl of online debate portals.
\end{abstract}

\begin{keywords}
  Touch{\'e} Task 1 \sep
  Argument Retrieval for Controversial Questions \sep
  Apache Lucene \sep
  Apache OpenNLP
\end{keywords}

\maketitle

\section{Introduction}

The task consists in creating an Informational Retrieval model that, given some controversial topics, retrieves us many relevant document as possible. The controversial nature of arguments makes the task more complex to solve, since the relevance of retrieved documents must be very accurate in order to not answer final users with some fake or dirty information.
Documents in the task are structured files with a set of predefined fields. For this task the corpus is the args.me.corpus (version 2020-04-01) and it is composed of five files:

\begin{itemize}
\item \verb|debateorg.json| with 338620 arguments;
\item \verb|debatepedia.json| with 21197 arguments;
\item \verb|debatewise.json| with 14353 arguments;
\item \verb|parliamentary.json| with 48 arguments;
\item \verb|idebate.json| with 13522 arguments
\end{itemize}

For a total of 387740 arguments. 
\medskip
\\To solve this task we used \textbf{Lucene}, a Java library providing powerful indexing and search features and \textbf{Apache OpenNLP} that is a machine learning library for the processing of natural language text. 
Through these tools we have tried to improve more and more the results obtained based on the qrels and topics of the year 2020 (in terms of relevant retrieved documents).

\section{Indexing}

This step is fundamental in order to guarantee an efficient and effective Information Retrieval system. Given its importance different setups were tested in order to decide which one was the best. These tests proceeded in an iterative way, starting from the most simple one to more complex configurations. Each configuration is represented by a particular Lucene Analyzer built with different components.

\subsubsection{Basic approach}

The most simple analyzer is Lucene StandardAnalyzer. It implements a canonical processing pipeline consisting of: tokenization, token normalization, lowercase transformation and stop-word removal. This approach allows very fast index computation, but it leads to poor quality results in terms of relevant documents.Due to the simple processing pipeline not very discriminant terms are placed inside the index, so queries cannot be matched effectively.

\subsubsection{Custom Analyzer approach}

In this phase of indexing improvement a more sophisticated approach was tried: the standard approach described above was enriched with further preprocessing stages. In particular the additional features tries to handle the following problems:

\begin{itemize}
    \item Apostrophes should not be considered. They normally do not add so much information. In order to handle them, an ApostropheFilter was added to the analyzer.
    \item Words like apple and apples or, politician and politicians are considered different words. In general this would be fine, but this implies that queries should be very specific. If, for example, a user looks for “very cute dogs”, but documents contain only word dog the query cannot be matched. This problem is faced applying a stemming step in the processing pipeline and this was done adding the EnglishMinimalStemFilter to the analyzer.
    \item Sometimes words could be written in camel case notation, contain alphanumeric characters or additional and useless blank spaces. All these variables could be considered as noise in tokens so they have to be handled and this was done adding the WordDelimiterGraphFilter to the analyzer.
\end{itemize}

\subsubsection{Stopwords}
To reduce the amount of memory required to store indexing we have inserted the StopWord elimination filter. In a collection of documents a lot of words are common and don’t give any additional information. These words can be articles, conjunctions or propositions and can be eliminated during indexing. 
In our project we have used the StopFilterFactory proposed by Lucene, it already includes a list of stopwords. What does this filter do? Let’s consider the following sentence: “I told you that she was not happy” with the Standard StopFilterFactory only the words “told” and “happy” will be indexed because the remaining words are not relevant.
As a first step we used the standard StopFilterFactory with the list provided by Lucene, but we realized that some not relevant words were still indexed and this also affected the query result so we decided to use a custom list (snowball.txt present in Hello-Tipster).
The result obtained with this list was better as we will see in the results reported in the next section

\subsubsection{OpenNLP approach}

Another implemented approach was the one based on OpenNLP library from Apache, a very powerful tool for natural language processing. Among the tools provided by OpenNLP we chose two of them: 
\begin{itemize}
    \item NER \verb|(Named Entity Recognition)|
    \item POS \verb|(Part of Speech)|
\end{itemize}
Our tests showed that this approach takes a lot of time to index documents, especially if they are very big (i.e. 7GB). Moreover results retrieved with this approach were not so different from the ones produced by Custom Analyzer.

\subsubsection{LMDirichlet approach}
Another approach we followed is the use of LMDirichlet as a similarity for Lucene. LMDirichlet permits the natural language process and we chose to use them since it was one of the best during last year's competition.
Using only the title in the topics this similarity was the one that gave us the best result.

\subsection{Searching}
Searching inherits the work done in the indexing phase, indeed each topic enters the same pipeline followed by documents. We decided to expand a little bit this idea, introducing the concept of query expansion. Once a topic has been processed by an analyzer, each of the survived tokens is substituted with a list of its synonyms (English synonyms). Working in this way, a topic can be matched more easily to documents and the results were better.\\
For each argument contained in the json files, the stance can be PRO or CON and it specifies if the text is in favor or not with the argument. To deserialize the JSON files we have used in our project the GSON Library (Developed by Google), this permits us to deserialize a big JSON file without consuming too much RAM.
Topics for the 2021 challenge are provided as an XML file by Touchè and it contains 50 topics. To deserialize it we have used the Jackson Core Library, it provides a set of functions that permit to deserialize an XML text by passing the models.
To test our software and the goodness of our solution we also use topics and qrels file of the last year and we evaluated our runs with the \verb|trec_eval| tool.
Our project was developed entirely in Java and makes use of some libraries like Lucene and Apache OpenNLP.

\cleardoublepage
\subsection{Result and Observations}
\subsubsection{Result}

As described above, we used different approaches to solve the problem and we compared them to get the best one.
At the beginning we checked manually the results retrieved by our system  verifying the consistency with reference to the query done.
In this way we immediately saw that some methods return stronger arguments than others.
For example, we noticed that adding stemming and stop words increase a lot the performance.
In order to have a more objective way to evaluate the runs we used the \verb|trec_eval| tool that accepts in input a run file and a qrels, it returns some statistics about the relevant documents, retrieved documents and retrieved relevant documents.


\begin{table}[ht]
    \centering
    \begin{tabular}{||c L L||} 
     \hline
     \multicolumn{1}{|m{3cm}|}{\textbf{Method}} & \multicolumn{1}{m{5.5cm}|}{\textbf{Pro}} & \multicolumn{1}{m{5.5cm}|}{\textbf{Cons}} \\ 
     \hline
     \multicolumn{1}{|m{3cm}|}{\textbf{Standard Analyzer}} & \multicolumn{1}{m{5.5cm}|}{Indexing is fast, there aren’t pre or post processing} & \multicolumn{1}{m{5.5cm}|}{ In the index are present lot of tokens that are not relevant to the document. Search returns also documents that are related to other topics.} \\ 
     \hline
     \multicolumn{1}{|m{3cm}|}{\textbf{Custom Analyzer}} & \multicolumn{1}{m{5.5cm}|}{ We have more control in what filters apply. Adding for example, stemming or apostrophes filters the performance increase.}  & \multicolumn{1}{m{5.5cm}|}{ The choice of filters to apply has a strong relation with obtained results. It’s important to choose a set of filters that allow a trade off between speed and accuracy. } \\
     \hline
     \multicolumn{1}{|m{3cm}|}{\textbf{Custom Analyzer (q. expansion with synonyms)}} & \multicolumn{1}{m{5.5cm}|}{ Using query expansion we can find more userful tokens in the index. This allows us to get more relevant documents.} & \multicolumn{1}{m{5.5cm}|}{ We need a file with synonyms, the quality of this file affects the goodness of data. } \\
     \hline
     \multicolumn{1}{|m{3cm}|}{\textbf{OpenNLP}} & \multicolumn{1}{m{5.5cm}|}{ In theory it should be the best in terms of accuracy.} & \multicolumn{1}{m{5.5cm}|}{ Very slow to index, in 8h has index only 2GB on the 7 provided. } \\ 
     \hline
     \multicolumn{1}{|m{3cm}|}{\textbf{LM Dirichlet}} & \multicolumn{1}{m{5.5cm}|}{ LM Dirichlet should improve the result since it should process natural language. } & \multicolumn{1}{m{5.5cm}|}{ Adding synonyms the results get worse. } \\ [1ex] 
     \hline
     \multicolumn{1}{|m{3cm}|}{\textbf{Boolean query in search}} & \multicolumn{1}{m{5.5cm}|}{ Allows you to use the description in the topics } & \multicolumn{1}{m{5.5cm}|}{ For task 2021 there was no description in the topics and the results with the topics of 2020 were not very good. } \\ [1ex] 
     \hline
    \end{tabular}
\caption{Pro and Cons for each approach}
\label{tab:my_label}
\end{table}

\begin{table}[ht]
    \centering
    \begin{tabular}{||c c L L L L L L||} 
     \hline
     \multicolumn{1}{|m{0.5cm}|}{\textbf{Run}} &
     \multicolumn{1}{m{3.5cm}|}{\textbf{Method}} & 
     \multicolumn{1}{m{1.5cm}|}{\textbf{Retrieved}} &
     \multicolumn{1}{m{1.5cm}|}{\textbf{Relevant}} &
     \multicolumn{1}{m{1.5cm}|}{\textbf{Rel. Ret.}} & 
     \multicolumn{1}{m{1cm}|}{\textbf{Set F}} & 
     \multicolumn{1}{m{1cm}|}{\textbf{MAP}} & 
     \multicolumn{1}{m{2.3cm}|}{\textbf{NDCG(CUT5)}} \\ 
     \hline
       \multicolumn{1}{|m{0.5cm}|}{2} &
       \multicolumn{1}{m{3.5cm}|}{\textbf{LMDirichlet}} &
       \multicolumn{1}{m{1.5cm}|}{24500} & 
       \multicolumn{1}{m{1.5cm}|}{932} & 
       \multicolumn{1}{m{1.5cm}|}{616} & 
       \multicolumn{1}{m{1cm}|}{0.0483} & 
       \multicolumn{1}{m{1cm}|}{0.2407} & 
       \multicolumn{1}{m{2.3cm}|}{0.4756} \\ [1ex]
     \hline
       \multicolumn{1}{|m{0.5cm}|}{3} &
       \multicolumn{1}{m{3.5cm}|}{\textbf{BM25}} & 
       \multicolumn{1}{m{1.5cm}|}{24500} & 
       \multicolumn{1}{m{1.5cm}|}{932} & 
       \multicolumn{1}{m{1.5cm}|}{579} & 
       \multicolumn{1}{m{1cm}|}{0.0454} & 
       \multicolumn{1}{m{1cm}|}{0.1830} & 
       \multicolumn{1}{m{2.3cm}|}{0.3554} \\ [1ex]
     \hline
       \multicolumn{1}{|m{0.5cm}|}{4} &
       \multicolumn{1}{m{3.5cm}|}{\textbf{LMDirichlet (q. exp.)}} &
       \multicolumn{1}{m{1.5cm}|}{24500} & 
       \multicolumn{1}{m{1.5cm}|}{932} & 
       \multicolumn{1}{m{1.5cm}|}{567} & 
       \multicolumn{1}{m{1cm}|}{0.0431} & 
       \multicolumn{1}{m{1cm}|}{0.1806} & 
       \multicolumn{1}{m{2.3cm}|}{0.3416} \\ [1ex]
     \hline
       \multicolumn{1}{|m{0.5cm}|}{1} &
       \multicolumn{1}{m{3.5cm}|}{\textbf{BM25 (q. exp.)}} & 
       \multicolumn{1}{m{1.5cm}|}{24500} & 
       \multicolumn{1}{m{1.5cm}|}{932} & 
       \multicolumn{1}{m{1.5cm}|}{533} & 
       \multicolumn{1}{m{1cm}|}{0.0418} & 
       \multicolumn{1}{m{1cm}|}{0.1470} & 
       \multicolumn{1}{m{2.3cm}|}{0.2511} \\ [1ex]
     \hline
     \multicolumn{1}{|m{0.5cm}|}{5} &
       \multicolumn{1}{m{3.5cm}|}{\textbf{Boolean query}} & 
       \multicolumn{1}{m{1.5cm}|}{24500} & 
       \multicolumn{1}{m{1.5cm}|}{932} & 
       \multicolumn{1}{m{1.5cm}|}{453} & 
       \multicolumn{1}{m{1cm}|}{0.0355} & 
       \multicolumn{1}{m{1cm}|}{0.1073} & 
       \multicolumn{1}{m{2.3cm}|}{0.1941} \\ [1ex]
     \hline
       \multicolumn{1}{|m{0.5cm}|}{6} &
       \multicolumn{1}{m{3.5cm}|}{\textbf{Boolean query (q.exp)}} & 
       \multicolumn{1}{m{1.5cm}|}{24500} & 
       \multicolumn{1}{m{1.5cm}|}{932} & 
       \multicolumn{1}{m{1.5cm}|}{522} & 
       \multicolumn{1}{m{1cm}|}{0.0409} & 
       \multicolumn{1}{m{1cm}|}{0.1046} & 
       \multicolumn{1}{m{2.3cm}|}{0.1651} \\ [1ex]
     \hline
    \end{tabular}
\caption{Best result obtained based on the TOPICS and QRELS of the year 2020 in order of MAP value, with query expansion we mean the use of synonyms in the query. To get these results we used the Custom Analyzer plus the method specified in the table.}
\label{tab:my_label}
\end{table}



\subsubsection{Analysis of the results}
\begin{figure}
    \scalebox{.8}
    \centering
    \includegraphics{student.eps}
    \caption{Runs in decreasing order of mean performance}
\end{figure}
Looking at the graph in \textit{Figure 1} and the \textit{Table 2} \textit{Boolean query solutions} achieved the worst result for any measurement system. Given this result it is more interesting to observe the results obtained through the two similarities \textbf{BM25} and \textbf{LMDirichtlet} and statistically observe which of the two obtained better results. Our studies have shown (\textit{figure 1}) that \textit{LMDirichtlet} produces better results than \textit{BM25} (both with and without query expansion).
Focusing only on the two similarities just mentioned, in \textit{figure 1} (in order of performance and in order of box plots) we find the following runs:
\begin{enumerate}
    \item LMDirichtlet without query expansion with mean = 0.2407;
    \item BM25 without query expansion with mean = 0.1830;
    \item LMDirichtlet with query expansion with mean = 0.1806;
    \item BM25 with query expansion with mean = 0.1470.
\end{enumerate}
\noindent
All data have been calculated on the MAP value returned by trec\_eval, but the same analysis can be done on other results such as ndcg, p@10 or others.
\noindent
Given the following data, we performed two different types of analyzes:
\begin{itemize}
    \item \textbf{Student’s t test};
    \item \textbf{Analysis of Variance (ANOVA)};
\end{itemize}
where:
\begin{itemize}
    \item \textbf{H0} (the null hypothesis) : is true when the means of the various runs are equal to each other (systems are equivalent);
    \item \textbf{H1} (the alternative hypothesis) : is true when the means of the various runs are not equal to each other (systems are not equivalent);
\end{itemize}



\subsubsection{LMDirichtlet vs BM25}
\textbf{Student's t test}
\medskip
\\Let's focus now on the first two box plots in \textit{figure 1}: we can see that both the first quartile and the median are higher in the left box plot, therefore on average the score obtained in topics using LMDirichtlet as similarity is better than BM25 (in the same condition). Then analyzing the first box plot we can see that the box has a greater height than the second, this suggests that there is greater variability in the score of the topics using LMDirichtlet. On the other hand, analyzing the second box plot starting from the left we can observe that there are 5 outliers corresponding to five topics that have obtained a very high score (this influencing the average in positive). 
\\To prove that LMDirichtlet provides statistically better results we performed the \textbf{Student's t test} to obtain the p-value. Taking into consideration the first and second run, precisely the one with LMDirichtlet and BM25 without query expansion, we obtained the following:
\begin{center}
    \[\textbf{pvalue} = 0.00092\]
\end{center}
\medskip
Since that the value obtained is very low then we can conclude that the difference observed between the means of the two runs is statistically significant.
Another way to observe the same result using the \textit{Student's t test} again is by rejecting the null hypothesis when:
\begin{center}
    \[t_{stat}  \geqslant t_{crit}\]
\end{center}
\noindent
In \textit{figure 2} we can see these results graphically, where:
\begin{itemize}
    \item t23: t statistics under H0 (2 = LMDirichtlet, 3 = BM25) 
    \item t24: t statistics under H0 (2 = LMDirichtlet, 4 = LMDirichtlet with query expansion) 
    \item t34: t statistics under H0 (3 = BM25, 4 = LMDirichtlet with query expansion) 
\end{itemize}
\begin{figure}
    \scalebox{.8}
    \centering
    \includegraphics{error.eps}
    \caption{Test Statistic}
\end{figure}

\noindent Statistically we have just shown that the method with Custom Analyzer with LMDirichtlet works better than Custom Analyzer with BM25. 
From \textit{figure 2} we can also graphically note that LMDirichtlet and LMDirichtlet with query expansion obtain statistically very different results.
The same method without query expansion is therefore better in terms of performance on relevant documents, below is the pvalue.
\begin{center}
    \[\textbf{pvalue} = 0.000173\]
\end{center}



\medskip
\textbf{ANOVA test}
\medskip
\\Let's focus now on all the results obtained from the various runs and see through the anova test whether or not we can accept the null hypothesis.
\textit{Table 3} shows the Anova table in which the absolute most important value is "Prob > F" which corresponds to the \textit{p-value}.
Since this value is very low we can say with certainty that the null hypothesis is not valid and that therefore there is at least one mean between the various runs that differs significantly from the others.
Certainly the best result is the one obtained with LMDirichtlet as similarity (without the use of synonyms on the query).
From the graph in \textit{figure 3} it is possible to see that the difference between BM25 (without query expansion) and LM Dirichtlet with query expansion is imperceptible. These two solutions in fact accept the hypothesis H0 in the student test as demonstrated also in the graph in \textit{figure 2} (the systems are similar/equivalent).
As previously mentioned, the graphs and data demonstrated in the document refer to the MAP returned by trec\_eval, the same analysis leads to the same results also using other measurement systems: the best run obtained is the one with Custom Analyzer plus LMDirichtlet as similarity without the use of synonyms in the query. In \textit{figure 4} and in \textit{figure 5} we can see the graphs obtained using ndcg as a measurement system, the pvalue obtained between LMDirichtlet and BM25 without query expansion is 0.000131.


\begin{figure}
    \centering
    \includegraphics{prova.eps}
    \caption{Multiple comparison of of the runs produced}
\end{figure}
\begin{center}
\begin{table}[ht]
    \centering
    \begin{tabular}{ |c|c|c|c|c|c| } 
     \hline
     \textbf{Source} & \textbf{SS} & \textbf{df} & \textbf{MS} & \textbf{F} & \textbf{Prob > F} \\ 
     \hline
     \textbf{Columns} & 0.6741 & 5 & 0.1348 & 4.9153 & 2.4543e-04 \\ 
     \hline
     \textbf{Error} & 8.0635 & 294 & 0.0274 & [] & [] \\ 
     \hline
    \textbf{Total} & 8.7375 & 299 & [] & [] & [] \\ 
     \hline
    \end{tabular}
    \caption{One-way Anova table}
    \label{tab:my_label}
\end{table}
\end{center}




\cleardoublepage
\subsection{Observation and perspectives for future work}
We can see that the choice of filters in the custom analyzer approach is very important for the efficiency of the retrieval system.
We have tried several ways to implement the indexer and the searcher, sometimes getting better results and sometimes getting even worse results than the standard analyzer.
Using a file with more words like “snowball.txt” instead of the default one helped us to increase the performance of the stop word filter.
Using synonyms often decrease the performance because more not relevant documents are retrieved.
To improve the system, a more extensive list of stop words could be found, in order to eliminate all those words that do not have any useful information for the text. Also trying new approaches in similarity and especially in stemming could help favor a better selection of documents.
Our results also show that the use of synonyms both in the query expansion and in the indexing phase worsens the results by returning less relevant documents.
To improve the system then we should analyze in which topic we have obtained bad results and try to improve these results, this would increase the map and all the other measurement systems.


\begin{figure}
    \centering
    \includegraphics{student2.eps}
    \caption{Box-plots using ndcg as measurement systems}
\end{figure}


\begin{figure}
    \centering
    \includegraphics{ANOVA2.eps}
    \caption{Multiple comparison of of the runs produced (using ndgc measurement systems)}
\end{figure}



\subsection{Acknowledgements}
A special thanks goes to Prof. \textit{Nicola Ferro} who helped us along this path by always guiding us in order to improve our solution. 
Through the course and also through this competition we have been able to improve our ability to search for documents, a skill that can be very useful in the working world where finding the relevant topic for the user is of fundamental importance. Thanks also to the group that worked hard during this project.


\section{References}
\begin{itemize}
    \item Project repository: \url{https://bitbucket.org/upd-dei-stud-prj/seupd2021-fpga/}
    \item Gson (Json): \url{https://mvnrepository.com/artifact/com.google.code.gson/gson}
    \item Jackson (Xml): \url{https://mvnrepository.com/artifact/com.fasterxml.jackson.dataformat/jackson-dataformat-xml}
    \item arg-framework: \url{https://git.webis.de/code-research/arguana/args/args-framework}
    \item Lucene: \url{https://lucene.apache.org/}
    \item OpenNLP: \url{https://opennlp.apache.org/}
\end{itemize}


\end{document}


