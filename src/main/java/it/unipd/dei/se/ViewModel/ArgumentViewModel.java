package it.unipd.dei.se.ViewModel;

import it.unipd.dei.se.Enum.StanceEnum;
import it.unipd.dei.se.Extensions.DoubleExtensions;
import it.unipd.dei.se.Extensions.ListExtensions;
import it.unipd.dei.se.Model.Argument;

import java.io.PrintWriter;
import java.util.List;

public class ArgumentViewModel {

    private String delimiter = "\n";

    private Argument argument;
    private String id;
    private String title;
    private String text;
    private String stance;
    private String topic;
    private String premises;
    private String author;
    private String conclusion;
    private double score;
    private Integer LucineDocID;
    private int position;

    public ArgumentViewModel(Argument argument,double score, Integer LucineDocID, int position)
    {
        if(argument == null)
            throw new IllegalArgumentException("argument cannot be null");

        this.argument = argument;
        this.score = score;
        this.LucineDocID = LucineDocID;
        this.position = position;

        id = argument.id;
        title = argument.context.sourceTitle;
        text = argument.context.sourceText;
        topic = argument.context.topic;
        conclusion = argument.conclusion;
        author = argument.context.author;

        if(!ListExtensions.isNullOrEmpty(argument.premises)) {
            var premise = argument.premises.get(0);
            premises = premise.text;
            stance = premise.stance;
        }
    }

    public void print(PrintWriter out)
    {

        String[] parameters = new String[]
                {
                        "score: " + DoubleExtensions.round(score, 2),
                        "Lucene ID: " + LucineDocID,
                        "topic: " + topic,
                        "text:  " + premises,
                        "conclusion: " + conclusion,
                        "position: " + position,
                        "id: " + id
                };
        if(LucineDocID == null)
        {
            parameters = new String[]
                    {
                            "score: " + DoubleExtensions.round(score, 2),
                            "topic: " + topic,
                            "text:  " + premises,
                            "conclusion: " + conclusion,
                            "position: " + position,
                            "id: " + id
                    };
        }

        String toPrint = String.join(delimiter, parameters);
        out.println(toPrint);
        out.println("----------------------------------");
    }



}
