package it.unipd.dei.se.Extensions;

//String class cannot be extended (final class)


import it.unipd.dei.se.Model.Argument;

public class StringExtensions{

    public static Boolean isNullOrEmpty(String string)
    {
        if(string == null || string.isEmpty())
            return true;
        return false;
    }

    public static String Join(String delimiter,String [] strings)
    {
        return String.join(delimiter, strings);
    }

    public static String ConvertToDateFormat(String string)
    {
        if(isNullOrEmpty(string))
            return string;

        String date = string.substring(0,9);
        return date;
    }

    public static String getDocumentTitle(Argument argument)
    {
        String stance = "";

        if(!argument.premises.isEmpty())
            stance = argument.premises.get(0).stance;

        String [] parameters = new String[]
                {
                        "id: " + argument.id,
                        "source: " + argument.context.sourceTitle,
                        "topic: "  + argument.context.topic,
                        "stance: " + stance,
                        "date: "   + StringExtensions.ConvertToDateFormat(argument.context.date)
                };


        String titleOfDocument = Join(", ",parameters);

        return titleOfDocument;
    }

}
