package it.unipd.dei.se.Model;

import org.apache.lucene.document.Document;

public class RetrievedDocument {

    public Integer ID; //LUCINE ID, (INTEGER) CAN BE NULLABLE
    public Document document;
    public double score;
    public int position;

    public RetrievedDocument(Document document, double score, Integer ID, int position)
    {
        this.ID = ID;
        this.document = document;
        this.score = score;
        this.position = position;
    }
}
