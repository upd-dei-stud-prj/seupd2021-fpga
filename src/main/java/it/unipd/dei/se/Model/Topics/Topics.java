package it.unipd.dei.se.Model.Topics;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Topics {
    @JacksonXmlProperty(localName = "topic")
    @JacksonXmlCData
    @JacksonXmlElementWrapper(useWrapping = false)
    public List<Topic> Topics;
}
