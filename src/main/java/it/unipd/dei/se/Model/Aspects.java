package it.unipd.dei.se.Model;

public class Aspects {
        public String name;
        public String weight;
        public String normalizedWeight;
        public String rank;
        public String aspectSpace;
        public String aspectSpaceCartesianX;
        public String aspectSpaceCartesianY;
        public String aspectSpaceBarycentricCoordinates;
}
