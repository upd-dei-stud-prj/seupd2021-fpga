package it.unipd.dei.se.Model;

import java.util.List;

public class Context {
    public List<Aspects> aspects;

    //author context
    public String author;
    public String authorOrganization;
    public String authorRole;
    public String authorImage;

    //other information
    public String mode;
    public String topic;
    public String sourceTitle;
    public String sourceUrl;
    public String sourceDomain;
    public String sourceId;
    public String sourceText;
    public String sourceTextConclusionStart;
    public String sourceTextConclusionEnd;
    public String sourceTextPremiseStart;
    public String sourceTextPremiseEnd;
    public String nextArgumentInSourceId;
    public String previousArgumentInSourceId;
    public String acquisitionTime;
    public List<String> corpora;
    public String date;

}


