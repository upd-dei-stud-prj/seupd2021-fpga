package it.unipd.dei.se.Model;

import it.unipd.dei.se.Model.Context;
import it.unipd.dei.se.Model.Premise;

import java.util.ArrayList;
import java.util.List;


public class Argument {
    public String id;
    public String conclusion;
    public List<Premise> premises;
    public Context context;

    public Argument(){
        this.context = new Context();
        this.premises = new ArrayList<>();
    }
}
