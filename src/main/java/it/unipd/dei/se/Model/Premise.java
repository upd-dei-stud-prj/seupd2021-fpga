package it.unipd.dei.se.Model;

import java.util.List;

public class Premise {
    public String text;
    public String stance;
    public List<PremiseAnnotation> annotations;
}
