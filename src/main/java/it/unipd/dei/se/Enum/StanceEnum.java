package it.unipd.dei.se.Enum;

public enum StanceEnum {
    PRO,
    CON,
    OTHER,
    ALL
}
