package it.unipd.dei.se.Enum;

import java.util.ArrayList;
import java.util.List;

public class PathOfFiles {
    public static final String IDEBATE = "experiment/idebate.json";
    public static final String IDEBATE_ARRAY = "experiment/idebateArray.json";
    public static final String PARLIAMENTARY_ARRAY = "experiment/parliamentaryArray.json";
    public static final String PARLIAMENTARY = "experiment/parliamentary.json";
    public static final String DEBATEWISE = "experiment/debatewise.json";
    public static final String DEBATEWISE_ARRAY = "experiment/debatewiseArray.json";
    public static final String DEBATEPEDIA = "experiment/debatepedia.json";
    public static final String DEBATEPEDIA_ARRAY = "experiment/debatepediaArray.json";
    public static final String DEBATEORG = "experiment/debateorg.json";
    public static final String DEBATEORG_ARRAY = "experiment/debateorgArray.json";

    public static ArrayList<String> getInputFiles(boolean getAll)
    {
        ArrayList<String> list = new ArrayList<>();
        list.add(PARLIAMENTARY_ARRAY);
        if(getAll) {
            list.add(IDEBATE_ARRAY);
            list.add(DEBATEWISE_ARRAY);
            list.add(DEBATEPEDIA_ARRAY);
            list.add(DEBATEORG_ARRAY);
        }

        return list;
    }





}


