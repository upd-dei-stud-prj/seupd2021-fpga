package it.unipd.dei.se.Core;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import it.unipd.dei.se.Extensions.PathOfFiles;
import it.unipd.dei.se.Extensions.ArgumentsExtensions;
import it.unipd.dei.se.Model.Argument;
import it.unipd.dei.se.Model.RetrievedDocument;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Indexer {

    private static final String DEFAULT_INDEX_PATH = "experiment/index";

    private static Path indexPath;
    private static ArrayList<String> sourceFiles;
    private static ArrayList<Argument> all_arguments;
    private static int LuceneID;


    public Indexer(boolean getAllDocuments) {
        final FileSystem fs = FileSystems.getDefault();
        this.indexPath = fs.getPath(DEFAULT_INDEX_PATH);
        this.LuceneID = 1;

        //If getAllDocuments is false I will only load the parliamentary file
        this.sourceFiles = PathOfFiles.getInputFiles(getAllDocuments);

        this.all_arguments = new ArrayList<>();
    }

    public void computeIndexing(final Analyzer analyzer) throws IOException {

        final Directory directory = FSDirectory.open(indexPath);
        //CustomAnalyzer buildAnalyzer = analyzer.build();

        final IndexWriterConfig config = new IndexWriterConfig(analyzer);

        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        config.setUseCompoundFile(false);
        config.setSimilarity(new BM25Similarity());

        final IndexWriter writer = new IndexWriter(directory, config);

        for(String file : sourceFiles)
        {
            System.out.println("\nprocessing "+file+"...");
            Stack<Document> docs = parseArguments(file,analyzer,writer);
            index(docs,writer);
        }

        writer.close();
        directory.close();
    }

    private static Stack<Document> parseArguments(String sourceFile,final Analyzer analyzer, IndexWriter writer) throws IOException
    {
        final Stack<Document> docs = new Stack<Document>();
        final FileSystem fs = FileSystems.getDefault();
        final Path path = fs.getPath(sourceFile);

        InputStream inputStream = Files.newInputStream(Path.of(sourceFile));

        //Skip the first 13 characters of the file because I want a json list and not a json object
        inputStream.skip(13);

        JsonReader reader = new JsonReader(new InputStreamReader(inputStream));

        try
        {
            reader.beginArray();
            while (reader.hasNext()) {

                //Convert json to an Argument object
                Argument argument = new Gson().fromJson(reader, Argument.class);

                //Convert the argument to a Lucene Document object
                Document d  = ArgumentsExtensions.getDocumentFromArgument(argument,sourceFile,Integer.toString(LuceneID),argument.premises.get(0).stance);

                docs.push(d);
                LuceneID++;

                //I will index 1000 documents at a time
                if(docs.size()>1000)
                {
                    index(docs,writer);
                }

                if(!reader.hasNext())
                    break;
            }
            reader.endArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch(IllegalStateException exception)
        {
            System.out.println(sourceFile + " is done");
        }

        return docs;
    }

    public static void index(final Stack<Document> docs, IndexWriter writer) throws IOException {
        while (!docs.isEmpty()) {
            Document d = docs.pop();
            writer.addDocument(d);
        }
    }

    //Method used to retrive informations about a document.
    //In our case we need this method to load the ArgumentViewModel object an it is used to print the details of a document to the user
    public Argument findAndPrintDocument(RetrievedDocument doc) throws IOException {
        Gson gson = new Gson();
        String url = "https://www.args.me/api/v2/arguments/" + doc.document.get("id") + "?format=json";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        String responseBody = response.toString();
        Argument myResponse = gson.fromJson(responseBody, Argument.class);

        if(myResponse == null){
            myResponse = new Argument();
        }

        in.close();

        return myResponse;
    }

}
