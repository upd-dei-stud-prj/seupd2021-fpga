package it.unipd.dei.se.Core;

import java.io.*;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import it.unipd.dei.se.Enum.DocumentFieldsEnum;
import it.unipd.dei.se.Extensions.ArgumentsExtensions;
import it.unipd.dei.se.Model.Topics.Topic;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.synonym.SynonymGraphFilterFactory;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.document.Document;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import it.unipd.dei.se.Extensions.StringExtensions;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import javax.validation.constraints.Null;

public class Searcher {

    private static final String DEFAULT_INDEX_PATH = "experiment/index";
    private static final String DEFAULT_RUN_FILE = "experiment/run.txt";
    private static final String DEFAULT_SOURCE_TOPICFILE = "experiment/topics-task-2.xml";

    private static final int MAX_DOCS_RETRIEVED = 20;
    private static final int MAX_DISTINCT_DOCS_RETRIEVED = 20;

    private final Path indexPath;
    private final Path runPath;

    private final String sourceTopicFile;
    private final PrintWriter run;

    public Searcher(String indexPath, String runFile, String sourceFile) throws IOException{

        final FileSystem fs = FileSystems.getDefault();

        if(StringExtensions.isNullOrEmpty((indexPath))){
            indexPath = DEFAULT_INDEX_PATH;
        }
        if(StringExtensions.isNullOrEmpty(runFile)){
            runFile = DEFAULT_RUN_FILE;
        }

        this.sourceTopicFile = DEFAULT_SOURCE_TOPICFILE;
        this.indexPath = fs.getPath(indexPath);

        if(Files.notExists(this.indexPath)){
            Files.createDirectory((this.indexPath));
        }

        if (!Files.isDirectory(this.indexPath)) {
            throw new IllegalStateException(
                    String.format("%s is not a directory.", this.indexPath.toAbsolutePath().toString()));
        }

        this.runPath = fs.getPath(runFile);

        run = new PrintWriter(Files.newBufferedWriter(runPath, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));

        System.out.printf("%n------------- INITIALIZING -------------%n");
        System.out.printf("- Index stored at: %s%n", this.indexPath.toAbsolutePath().toString());
        System.out.printf("- Run file at: %s%n", runPath.toAbsolutePath().toString());
        System.out.printf("----------------------------------------%n");

    }

    //If I'm not reading arguments from the main I will create an object with null values and the method will loads the default files path
    public Searcher() throws IOException{
        this(null,null,null);
    }


    public void search(final String TopicID, final Topic topic, final Analyzer analyzer, ArgumentsExtensions argumentsExtensions)
            throws IOException, ParseException {

        System.out.printf("\n ------------- SEARCHING DOCUMENTS FOR QUERY %s ------------------- \n", TopicID);

        //builderAnalyzer.addTokenFilter(SynonymGraphFilterFactory.class, "synonyms", "synonyms.txt");

        final Directory directory = FSDirectory.open(indexPath);
        final DirectoryReader reader = DirectoryReader.open(directory);

        final IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(new BM25Similarity());

        final QueryParser qp = new QueryParser(DocumentFieldsEnum.BODY, analyzer);

        BooleanQuery.Builder bq = new BooleanQuery.Builder();
        bq.add(qp.parse(QueryParserBase.escape(topic.title)), BooleanClause.Occur.SHOULD);
        bq.add(qp.parse(QueryParserBase.escape(topic.description)), BooleanClause.Occur.SHOULD);

        final Query q = bq.build();

        System.out.printf("- Query successfully parsed: %s%n", q.toString());
        final ScoreDoc[] hits = searcher.search(q, MAX_DOCS_RETRIEVED).scoreDocs;

        System.out.printf("- %d documents retrieved%n", hits.length);

        int nDocRetrieved = 0;
        HashMap<String,Document> distinctDocs = new HashMap<>();
        for(int i = 0; i < hits.length; i++){

            Document document = reader.document(hits[i].doc);
            String docID = document.get(DocumentFieldsEnum.ID);

            //Use HashMap to avoid adding multiple equals documents by checking the id of the document
            Document docAdded = distinctDocs.put(docID, document);

            //If document doesn't exists in the hashMap
            if(docAdded == null){
                var score = hits[i].score;
                run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", TopicID, docID, i, score, DocumentFieldsEnum.RUNID);
                System.out.printf(Locale.ENGLISH, "   %s\tQ0\t%s\t%d\t%.6f\t%s%n", TopicID, docID, i, score, DocumentFieldsEnum.RUNID);
                argumentsExtensions.addRetrieved(document,score, i);

                if(++nDocRetrieved == MAX_DISTINCT_DOCS_RETRIEVED){
                    break;
                }
            }
        }


        run.flush();
        reader.close();
        directory.close();
        System.out.printf("- Searcher successfully closed%n");

        System.out.printf("-------------------------------------------------------------%n");
    }

    public void close() {

        System.out.printf("%n------------- CLOSING -------------%n");

        run.close();

        System.out.printf("- Run file closed%n");
        System.out.printf("-----------------------------------%n");

        System.out.printf("%n%n############ BYE BYE, IR! ############%n%n");
    }

    //Load the list of topics from the file
    public List<Topic> parseTopics()
    {
        final ArrayList<Topic> Topics = new ArrayList<>();
        try {
            XmlMapper xmlMapper = new XmlMapper();
            String readContent = new String(Files.readAllBytes(Paths.get(sourceTopicFile)));
            it.unipd.dei.se.Model.Topics.Topics deserializedData = xmlMapper.readValue(readContent, it.unipd.dei.se.Model.Topics.Topics.class);

            if(deserializedData != null)
            {
                return deserializedData.Topics;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return Topics;
    }

    public void cleanIndexFolder() throws IOException {
        File directory = new File(DEFAULT_INDEX_PATH);
        FileUtils.cleanDirectory(directory);
    }

    public static boolean indexFolderIsEmpty(){

        final FileSystem fs = FileSystems.getDefault();
        Path index = fs.getPath(DEFAULT_INDEX_PATH);

        if (Files.isDirectory(index)) {
            try (DirectoryStream<Path> directory = Files.newDirectoryStream(index))
            {
                return !directory.iterator().hasNext();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return false;
    }
}